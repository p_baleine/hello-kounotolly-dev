# hello-kounotolly-dev

## ステージング環境での起動

```bash
$ git clone https://p_baleine@bitbucket.org/p_baleine/hello-kounotolly-dev.git
$ cd hello-kounotolly-dev
$ nvm use
$ npm install
$ cp config/default.json config/staging.json # config/staging.jsonを編集
$ NODE_ENV=staging make migrate
$ NODE_ENV=staging nohup node index.js &
```

# TODO

* `nohup`以外の方法
