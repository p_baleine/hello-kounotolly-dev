var express = require('express');
var config = require('config');
var knex = require('knex').initialize(config.database);

var app = express();

app.get('/', function(req, res) {
  knex('test')
    .select()
    .then(function(records) {
      res.send(records[0].content);
    });
});

if (!module.parent) {
  app.listen(config.port);
  console.log('listening on port %d', config.port);
}
