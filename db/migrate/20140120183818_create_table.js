
exports.up = function(knex, Promise) {
  return knex.schema.createTable('test', function(t) {
    t.engine('InnoDB');
    t.increments().primary();
    t.string('content');
  })
    .then(function() {
      return knex('test').insert({ content: 'Hello, Kounotolly' });
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('test');
};
